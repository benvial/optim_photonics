
========================
Optimization photonics
========================

.. .. image:: https://img.shields.io/endpoint?url=https://gitlab.com/benvial/refidx/-/jobs/artifacts/main/file/logobadge.json?job=badge&labelColor=8f8f8f
..   :target: https://gitlab.com/benvial/refidx/-/releases
..   :alt: Release
.. 
.. 
.. .. image:: https://img.shields.io/gitlab/pipeline-status/benvial/refidx?branch=main&logo=gitlab&labelColor=dedede&logoColor=ffffff&style=for-the-badge
..   :target: https://gitlab.com/benvial/refidx/commits/main
..   :alt: pipeline status
.. 
.. 
.. .. image:: https://img.shields.io/gitlab/coverage/benvial/refidx/main?logo=python&logoColor=e9d672&style=for-the-badge
..  :target: https://benvial.gitlab.io/refidx/coverage.html
..  :alt: coverage report 
.. 
.. .. image:: https://img.shields.io/pypi/v/refidx?logo=python&logoColor=e9d672&style=for-the-badge
..   :target: https://pypi.org/project/refidx/
..   :alt: pypi
.. 
.. .. image:: https://img.shields.io/pypi/dm/refidx?logo=python&logoColor=e9d672&style=for-the-badge
..  :target: https://pypi.org/project/refidx/
..  :alt: pypi-dl
.. 
.. .. image:: https://img.shields.io/badge/code%20style-black-dedede.svg?logo=python&logoColor=e9d672&style=for-the-badge
..   :target: https://black.readthedocs.io/en/stable/
..   :alt: Code style: black
.. 
.. .. image:: https://img.shields.io/badge/license-GPLv3-blue?color=5faa9c&logo=open-access&logoColor=white&style=for-the-badge
..   :target: https://gitlab.com/benvial/refidx/-/blob/main/LICENSE.txt
..   :alt: license 
.. 

This is the code to generate results for the paper "B.Vial & Y. Hao, Open source computational photonics with auto differentiable topology optimization".

A HTML version of the paper can be found `here <paper/index.html>`_

 .. Version |release|, last updated on |today|.

 .. toctree::
    :maxdepth: 2
    :caption: Code
    :hidden:

    code/index
   



  
