


# Open source computational photonics with auto differentiable topology optimization
## Benjamin Vial & Yang Hao


This repository contains the code to reproduce the results in the paper:

> Vial, B.; Hao, Y. Open-Source Computational Photonics with Auto Differentiable Topology Optimization. Mathematics 2022, 10, 3912. https://doi.org/10.3390/math10203912


One first needs to install the python packages for the different numerical methods. We use [``conda``](https://docs.conda.io/en/latest/). First create an environment:

```bash
conda env create -f environment.yml
```
Then one needs to activate it

```bash
conda activate optphot
```

Alternatively one could use [``docker``](https://www.docker.com/)

```bash
docker pull benvial/optphot
```
to download an image with the environment already setup.



## Running the code

> **Note**
> 
> All the steps are summarized in the Makefile. Just run
> ```bash
> make optim
> make postpro
> ```
> The build is also automated through [continuous integration](https://gitlab.com/benvial/optim_photonics/-/pipelines) (look inside .gitlab-ci.yml).


### FEM results

TE optimization and computing wavelength spectrum:

```bash
python code/gyptis/superscatterer.py --polarization TE
python code/gyptis/spectrum.py --polarization TE
```

The results will be saved in ``data/gyptis/TE``.

TM:

```bash
python code/gyptis/superscatterer.py --polarization TM
python code/gyptis/spectrum.py --polarization TM
```

The results will be saved in ``data/gyptis/TM``.

Then the postprocessing (field maps and modal analysis):

```bash
python code/gyptis/postpro.py
```

This will create `figs/figure1.png` (Figure 1 in the paper) and `figs/figure2.png` (Figure 2 in the paper)


### FMM results

Run the optimization:

```bash
python code/nannos/metasurface.py
```

The results will be saved in ``data/nannos``.


Postprocessing:

```bash
python code/nannos/postpro.py
```


This will create `figs/figure3.png` (Figure 3 in the paper) and `figs/figure4.png` (Figure 4 in the paper)



### PWEM results

Run the optimization:



```bash
# for TE
python code/protis/phc.py -p TE -o bandgap -i random -e 4
# for TM
python code/protis/phc.py -p TM -o dispersion -i random -e 5
```

The results will be saved in ``data/protis``.

Postprocessing:

```bash
python code/protis/postpro.py
```


This will create `figs/figure5.png` (Figure 5 in the paper).


<!-- 
# Links

[Download article](https://gitlab.com/benvial/optim_photonics/-/jobs/artifacts/main/raw/paper/build/article.pdf?job=paper)

[HTML version](https://benvial.gitlab.io/optim_photonics/)

[Download sources](https://gitlab.com/benvial/optim_photonics/-/jobs/artifacts/main/download?job=paper) -->
