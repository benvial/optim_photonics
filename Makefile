
.PHONY: simu postpro paper doc

SHELL := /bin/bash
MAKEFLAGS += -j5

HOSTING = gitlab
VERSION=$(shell python3 -c "from configparser import ConfigParser; p = ConfigParser(); p.read('setup.cfg'); print(p['metadata']['version'])")
BRANCH=$(shell git branch --show-current)
GITLAB_PROJECT_ID=36249996


## Push to gitlab
save:
	@echo "Pushing to gitlab..."
	git add -A
	@read -p "Enter commit message: " MSG; \
	git commit -a -m "$$MSG"
	git push origin main


## Clean generated files
clean:
	$(call message,${@})
	@find . | grep -E "(data|figs|nohup.out|jitfailure*|__pycache__|\.pyc|\.pyo$\)" | xargs rm -rf
	@rm -rf paper/build
	@rm -rf public
	@rm -rf site
	@cd doc && $(MAKE) -s clean

environment:
	@conda config --add channels conda-forge && \
	conda install mamba  && \
	mamba env create -f environment.yml

watch:
	@watch tail -n 30 nohup.out

define initenv
	@source activate optphot && \
	export OMP_NUM_THREADS=1 && \
	export OPENBLAS_NUM_THREADS=1 && \
	export GOTO_NUM_THREADS=1 && \
	export MKL_NUM_THREADS=1 && \
	export MPLBACKEND=agg && \
	export GYPTIS_ADJOINT=1 && \
	export PYTHONUNBUFFERED=1
endef

define checkversions
	echo "gyptis version" && python -c "import gyptis;print(gyptis.__version__)" && \
	echo "nannos version" && python -c "import nannos;print(nannos.__version__)" && \
	echo "protis version" && python -c "import protis;print(protis.__version__)"
endef

env:
	$(call initenv)


define runner
	$(call initenv) && nohup python $(1)
endef


gyptis_opt_TE:
	$(call runner,code/gyptis/superscatterer.py --polarization TE)

gyptis_opt_TM:
	$(call runner,code/gyptis/superscatterer.py --polarization TM)

nannos:
	$(call runner,code/nannos/metasurface.py)

protis_bandgap_TE:
	$(call runner,code/protis/phc.py -p TE -o bandgap -i random -e 4)

protis_dispersion_TM:
	$(call runner,code/protis/phc.py -p TM -o dispersion -i random -e 5)


simu: gyptis_opt_TE gyptis_opt_TM nannos protis_bandgap_TE protis_dispersion_TM
	@echo "done simulation"


postpro_gyptis:
	$(call runner,code/gyptis/postpro.py)

postpro_nannos:
	$(call runner,code/nannos/postpro.py)

postpro_protis:
	$(call runner,code/protis/postpro.py)

postpro: postpro_gyptis postpro_nannos postpro_protis
	@echo "done postprocessing"


cpfigs:
	@mkdir -p paper/src/figs
	@cp -rf figs/* paper/src/figs || echo no figures found

paper-pdf: cpfigs
	$(call initenv) && cd paper && make init && cd build && pdflatex article.tex && bibtex article &&  \
	pdflatex article.tex && pdflatex article.tex

paper-html: cpfigs
	$(call initenv) && (python -c "import panflute" | pip install panflute) && \
 	cd paper && make init && make html

html: paper-html doc
	@mkdir -p site/paper && cp -r paper/build/index.html paper/build/custom.css paper/build/figs site/paper/
	@cp -r doc/_build/html/* site/


devpaper:
	cd paper && make

doc-req:
	$(call initenv) && pip install -r doc/requirements.txt

doc:
	$(call initenv) && cd doc && make clean && make dochtml

all:
	$(MAKE) simu && \
	$(MAKE) postpro && \
	$(MAKE) paper && \
	$(MAKE) doc-req && \
	$(MAKE) doc && \
	@mkdir -p public/data && \
	@cp -r paper/build/index.html public && \
	@cp -r paper/build/custom.css public && \
	@cp -r paper/build/figs/ public/ && \
	@cp -r doc/_build/html/* public/data/

## Tag and push tags
tag:
	@if [ "$(shell git rev-parse --abbrev-ref HEAD)" != "main" ]; then exit 1; fi
	@echo "Version v$(VERSION)"
	@git add -A
	git commit -a -m "Publish v$(VERSION)"
	@git push origin $(BRANCH)
	@git tag v$(VERSION) || echo Ignoring tag since it already exists
	@git push --tags || echo Ignoring tag since it already exists on the remote

## Create a release
release:
	@pip install python-gitlab
	@if [ "$(shell git rev-parse --abbrev-ref HEAD)" != "main" ]; then exit 1; fi
	@gitlab project-release create --project-id $(GITLAB_PROJECT_ID) \
	--name "version $(VERSION)" --tag-name "v$(VERSION)" --description "Released version $(VERSION)"
