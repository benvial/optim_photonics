#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# License: MIT
"""
Postprocessing
===============

"""

from superscatterer import *
import matplotlib.colors as mplcolors


os.makedirs(f"figs", exist_ok=True)


def _get_power_dens(u, v):
    re = gy.as_vector([v[1].real, -v[0].real])
    im = gy.as_vector([v[1].imag, -v[0].imag])
    EcrossHstar = u * gy.Complex(re, -im)
    return (gy.Constant(0.5) * EcrossHstar).real


def scattering_cross_section_angular(self,Npts=361):
    uscatt = self.solution["diffracted"]

    pulsation = self.formulation.source.pulsation
    coeff = (
        1j * pulsation * gy.mu_0
        if self.formulation.polarization == "TM"
        else -1j * pulsation * gy.epsilon_0
    )
    grad_field = gy.grad(uscatt)
    re = gy.as_vector([grad_field[1].real, -grad_field[0].real])
    im = gy.as_vector([grad_field[1].imag, -grad_field[0].imag])
    vscatt = gy.Constant(1 / coeff) * gy.Complex(re, im)

    n_out = gy.dolfin.Expression(
        ("x[0]/Rcalc", "x[1]/Rcalc"),
        degree=self.degree,
        Rcalc=self.geometry.Rcalc,
    )
    Rcalc = self.geometry.Rcalc
    t = np.linspace(0,2*gy.pi,Npts)
    Sscatt = _get_power_dens(uscatt, vscatt)
    W = gy.dot(n_out, Sscatt)
    W = gy.project_iterative(W, Vplot)
    out =[]
    for _t in t:
        x,y = Rcalc*np.cos(_t), Rcalc*np.sin(_t)
        out.append(W(x,y))
    out = np.array(out) / self.time_average_incident_poynting_vector_norm
    return out

def make_movies(polarization):
    os.system(
        f"convert -delay 20 -loop 0 data/gyptis/{polarization}/dens*.png figs/animation_gyptis_{polarization}.gif"
    )
    os.system(
        f"convert figs/animation_gyptis_{polarization}.gif figs/animation_gyptis_{polarization}.mp4"
    )
    os.system(f"rm -f figs/animation_gyptis_{polarization}.gif")


for polarization in ["TE", "TM"]:

    print("building movie")
    make_movies(polarization)


fig = plt.figure(constrained_layout=True, figsize=(4.6, 2.5))
gs = fig.add_gridspec(2, 3)
ax0 = fig.add_subplot(gs[:, 0:2])
axTE = fig.add_subplot(gs[0, 2])
axTM = fig.add_subplot(gs[1, 2])


scat = Scatterer(R, Rcalc, lbox, lmin, ref_des)
scat.build()


for polarization in ["TE", "TM"]:

    compute_spectrum()

    arch = np.load(f"data/gyptis/{polarization}/opt_scatt.npz", allow_pickle=True)
    xopt = arch["xopt"]
    density_bin = arch["density_bin"]


    archspec = np.load(f"data/gyptis/{polarization}/opt_scatt_spectrum.npz", allow_pickle=True)
    wl = archspec["wl"]
    scs_norma = archspec["scs_norma"]

    simu = Simulation(scat, wavelength_target, polarization)
    opt = simu.optimizer
    density_bin = gu.array2function(density_bin, opt.fs_sub)
    Vplot = gy.dolfin.FunctionSpace(scat.geometry.mesh, "CG", 2)
    epsilon_design = simu.simp(density_bin)
    epsilon_design = gy.project_iterative(epsilon_design, Vplot)
    simu = Simulation(scat, wavelength_target, polarization, epsilon_design)

    if polarization == "TE":
        plt.sca(axTE)
        currax = axTE
        field = "$|H_z|^2$"
        id = "(b)"
    else:
        plt.sca(axTM)
        currax = axTM
        field = "$|E_z|^2$"
        id = "(c)"
    currax.annotate(
        id, (-0.2, 0.9), xycoords="axes fraction", fontsize=8, fontweight="400"
    )
    simu.epsilon_design = epsilon_design
    simu.solve()
    u = simu.simulation.solution["diffracted"]
    fieldplot = abs((u * u.conj).real)
    fieldplot = gy.project_iterative(fieldplot, opt.fs_ctrl)
    a = gy.dolfin.plot(
        fieldplot,
        cmap="inferno",
        edgecolors="face",
        # vmin=-3,vmax=None,
        norm=mplcolors.LogNorm(vmin=1e-2, vmax=None),
    )
    plt.colorbar(a, extend="min")
    gy.dolfin.plot(
        density_bin,
        cmap="Greys",
        edgecolors="face",
        alpha=0.2,
        lw=0,
        rasterized=True,
    )
    lx, ly = scat.geometry.box_size
    plt.xlim(-lx / 2, lx / 2)
    plt.ylim(-ly / 2, ly / 2)
    currax.annotate(field, (0.75, 0.87), xycoords="axes fraction", c="w", fontweight="400")
    plt.axis("off")
    gy.pause(0.1)

    plt.sca(ax0)
    color = "#6e8cd0" if polarization == "TE" else "#d0716e"

    plt.plot(wl * 1000, scs_norma, c=color)
    plt.xlabel("wavelength (nm)")
    plt.ylabel("normalized scattering width $\sigma_s/2R$")
    if polarization == "TE":
        inset = fig.add_axes([0.09, 0.77, 0.2, 0.2])
    else:
        inset = fig.add_axes([0.44, 0.77, 0.2, 0.2])

    circle = plt.Circle((0, 0), R, color=color, fill=False)
    pola_pos = (1.12, -0.12) if polarization =="TE" else (-0.12,-0.12)
    inset.annotate(polarization, pola_pos, xycoords="axes fraction", color=color)
    inset.add_patch(circle)
    inset.set_axis_off()
    p, cb = gy.plot(
        density_bin,
        cmap="Greys",
        edgecolors="face",
        lw=0,
        rasterized=True,
        ax=inset,
    )
    cb.remove()

    #################################################################
    #################################################################
    #################################################################
    #################################################################

    #
    # fig = plt.figure(constrained_layout=True, figsize=(4.6, 2.5))
    # gs = fig.add_gridspec(2, 3)
    # ax0 = fig.add_subplot(gs[:, 0:2])
    # axTE = fig.add_subplot(gs[0, 2])
    # axTM = fig.add_subplot(gs[1, 2])


    #### polar plots
    Npts=361*2
    t = np.linspace(0,2*gy.pi,Npts)
    scs = scattering_cross_section_angular(simu.simulation,Npts=Npts)
    sgn = -1 if polarization=="TE" else 1
    scs *= sgn
    if polarization == "TE":
        inset = fig.add_axes([0.09, 0.51, 0.2, 0.2], polar=True)
    else:
        inset = fig.add_axes([0.44, 0.51, 0.2, 0.2], polar=True)
    bullseye=3
    r_ = scs/(2*R)
    min10 = 20*np.log10(np.min(r_))
    min10 = -30
    max10 = 20
    r = 20*np.log10(r_) - min10 + bullseye
    inset.plot(t, r,color=color)
    # l = np.linspace(-30,20,6)
    l = np.linspace(-20,20,3)
    inset.set_rticks(l - min10 + bullseye)
    inset.set_yticklabels(["%d" % x for x in l],fontsize=3)
    thetaticks = np.arange(0,360,90)

    # set ticklabels location at 1.3 times the axes' radius
    inset.set_thetagrids(thetaticks,fontsize=3)
    inset.set_thetagrids(thetaticks,fontsize=3)
    inset.xaxis.set_tick_params(pad=-7)
    inset.set_rlim(0, max10 - min10 + bullseye)
    # ax.set_title(polarization)
    inset.xaxis.grid(linewidth=0.1)
    inset.yaxis.grid(linewidth=0.1)
    # plt.tight_layout()


ax0.annotate(
    "(a)", (-0.15, 0.95), xycoords="axes fraction", fontsize=8, fontweight="400"
)
maxi = np.max(scs_norma) * 1.1
ax0.vlines(wavelength_target * 1000, 0, maxi, ls="--", color="#a4a4a4")
ax0.set_ylim(0, maxi)
# plt.tight_layout()
plt.savefig(f"figs/figure1.eps")
plt.savefig(f"figs/figure1.png")



############################
# modal analysis
############################

fig_mode, ax_mode = plt.subplots(1, 2, figsize=(5, 3))
ax_mode[0].set_xlabel("wavelength (nm)")
ax_mode[0].set_ylabel("coupling coefficient $C_n$")
ax_mode[1].set_xlabel("wavelength (nm)")
ax_mode[1].set_ylabel("coupling coefficient $C_n$")
ax_mode[0].set_title("TE")
ax_mode[1].set_title("TM")
plt.tight_layout()
PNS = []

for polarization in ["TE", "TM"]:

    arch = np.load(f"data/gyptis/{polarization}/opt_scatt.npz", allow_pickle=True)
    density_bin = arch["density_bin"]
    simu = Simulation(scat, wavelength_target, polarization)
    opt = simu.optimizer
    density_bin = gu.array2function(density_bin, opt.fs_sub)
    Vplot = gy.dolfin.FunctionSpace(scat.geometry.mesh, "CG", 2)
    epsilon_design = simu.simp(density_bin)
    epsilon_design = gy.project_iterative(epsilon_design, Vplot)
    simu = Simulation(scat, wavelength_target, polarization, epsilon_design)
    # modes
    print("computing modes")
    neig = 9
    simu.eigensolve(
        neig, wavevector_target=2 * np.pi / wavelength_target, tol=1e-6
    )  # )
    ev = simu.solution["eigenvalues"]
    modes = simu.solution["eigenvectors"]
    s_modal = simu.simulation
    form = s_modal.formulation
    form.source_domains = ["design"]
    xi = form.xi.as_subdomain()
    chi = form.chi.as_subdomain()
    xi_a = form.xi.build_annex(
        domains=form.source_domains, reference=form.reference
    ).as_subdomain()
    chi_a = form.chi.build_annex(
        domains=form.source_domains, reference=form.reference
    ).as_subdomain()

    xi_dict = form.xi.as_property()
    chi_dict = form.chi.as_property()
    xi_a_dict = form.xi.build_annex(
        domains=form.source_domains, reference=form.reference
    ).as_property()
    chi_a_dict = form.chi.build_annex(
        domains=form.source_domains, reference=form.reference
    ).as_property()

    Kns = []

    print("computing norms")
    for w, v in zip(ev, modes):
        Kn = gy.assemble(gy.dot(chi * v, v) * form.dx)
        Kns.append(Kn)
        lamb = 2 * np.pi / w.real
        Q = 0.5 * w.real / w.imag
        # qnm = v / Kn**0.5
        # Un = gy.project_iterative(qnm.real, Vplot)
        # plt.figure()
        # gy.plot(Un, cmap="RdBu_r", ax=plt.gca())
        # plt.axis("off")
        # plt.title(rf"$\lambda=$ {lamb:.4f}, $Q=$ {Q:.2f}")
        # plt.pause(0.1)

    def get_coupling_coeff(scatt, mode_index, pw, Kn=None):
        ev = scatt.solution["eigenvalues"]
        modes = scatt.solution["eigenvectors"]
        vn = modes[mode_index]
        kn = ev[mode_index]
        k = pw.wavenumber
        if Kn is None:
            Kn = gy.assemble(gy.dot(chi * vn, vn) * form.dx)

        source = form.maxwell(
            pw.expression,
            vn,
            xi_dict["design"] - xi_a_dict["design"],
            chi_dict["design"] - chi_a_dict["design"],
            domain="design",
        )
        vt = -source[0] + gy.Constant(k) ** 2 * source[1]
        # surf_term = -gy.dot((xi_dict["design"] - xi_a_dict["design"]) *gy.grad(pw.expression),scat.geometry.unit_normal_vector)*vn
        # st = surf_term("+")*form.dS("des_bnds")
        VT = gy.assemble(vt)
        # VT += ST
        Jn = VT / Kn
        Pn = Jn / (k**2 - kn**2)
        return Pn.real + 1j * Pn.imag

    wls = np.linspace(0.55, 0.65, 51)

    lambdas = 2 * np.pi / ev.real
    Qs = 0.5 * ev.real / ev.imag

    coupling_mode = []
    # modes_exp= [0, 1, 2]
    modes_exp = range(neig)

    print("computing coupling")
    for mode_index in modes_exp:
        vn = modes[mode_index]
        Kn = Kns[mode_index]
        coupling = []
        for wavelength in wls:
            pw = gy.PlaneWave(
                wavelength=wavelength,
                angle=gy.pi / 2,
                dim=2,
                domain=scat.geometry.mesh,
                degree=2,
            )
            Pn = get_coupling_coeff(s_modal, mode_index, pw, Kn=Kn)
            coupling.append(Pn)
        coupling_mode.append(coupling)
    coupling_mode = np.array(coupling_mode).T

    stot = np.sum(np.abs(coupling_mode), axis=1)

    q = np.array([np.abs(coupling_mode)[i] for i in range(len(wls))])

    PNS.append(q)

    colors = plt.rcParams["axes.prop_cycle"].by_key()["color"]

    ii = np.where(abs(wls - 0.6) < 1e-6)[0]
    modes_exp = np.flipud(np.argsort(q[ii, :][0]))[:4]

    iplt = 0 if polarization == "TE" else 1

    ax_mode[iplt].plot(1000 * wls, q[:, modes_exp])
    ax_mode[iplt].set_ylim(None,1.43*np.max(q))

    index_label = [16, 5, -16, -8] if polarization == "TE" else [5, 5, 20, 25]

    # modes_exp= range(neig)
    imodplot = 0
    for mode_index in modes_exp:
        color = colors[imodplot]
        w = ev[mode_index]
        v = modes[mode_index]
        Kn = Kns[mode_index]
        inset = fig_mode.add_axes([0.5*iplt+0.08 + 0.092 * imodplot, 0.70, 0.14, 0.14])
        # _, inset = plt.subplots()
        plt.sca(inset)
        lamb = 2 * np.pi / w.real
        Q = 0.5 * w.real / w.imag
        qnm = v / Kn**0.5
        Un = gy.project_iterative(qnm.real, Vplot)
        gy.dolfin.plot(Un, cmap="RdBu_r")
        gy.dolfin.plot(
            density_bin,
            cmap="Greys",
            edgecolors="face",
            alpha=0.2,
            lw=0,
            rasterized=True,
        )
        inset.set_axis_off()
        lx, ly = scat.geometry.box_size
        plt.xlim(-lx / 2, lx / 2)
        plt.ylim(-ly / 2, ly / 2)
        inset.set_title(
            f"$\lambda_{imodplot+1}=$ {1000*lamb:.0f}nm\n$Q_{imodplot+1}=$ {Q:.1f}", fontsize=4, c=color
        )

        xannot = 1000 * wls[index_label[imodplot]]
        yannot = q[index_label[imodplot], mode_index]
        ax_mode[iplt].annotate(
            f"{imodplot+1}",
            (xannot, yannot),
            color=color,
            bbox=dict(boxstyle="square,pad=0.2", fc="w", ec="none"),
        )
        plt.pause(0.1)
        imodplot += 1


plt.savefig(f"figs/figure2.eps")
plt.savefig(f"figs/figure2.png")
