#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# License: MIT
"""
Spectrum
========

"""

from superscatterer import *

import argparse
parser = argparse.ArgumentParser()
parser.add_argument(
    "-p", "--polarization", default="TE", help="polarization case (TE or TM)"
)
args = parser.parse_args()
polarization = args.polarization


################################################################
#

if __name__ == "__main__":
    compute_spectrum(polarization)
