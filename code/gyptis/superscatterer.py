# -*- coding: utf-8 -*-
"""
Topology optimization
=====================

Superscatterer for each polarization.

To run::

    python superscatterer --polarization TE
    python superscatterer --polarization TM

"""

import os

os.environ["GYPTIS_ADJOINT"] = "1"
import argparse
import numpy as np
import matplotlib.pyplot as plt
import gyptis as gy
import gyptis.optimize as go
import gyptis.utils as gu
import refidx as ri

# matplotlib config
plt.close("all")
plt.ion()

################################################################
# Parse arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    "-p", "--polarization", default="TE", help="polarization case (TE or TM)"
)
args = parser.parse_args()
polarization = args.polarization

################################################################
# Create data directory
datadir = f"data/gyptis/{polarization}"
os.makedirs(datadir, exist_ok=True)

################################################################
# FEM parameters
wavelength_target = 0.6
degree = 2
pmesh = 10

################################################################
# Materials
db = ri.DataBase()
material0 = db.materials["main"]["SiO2"]["Lemarchand"]
eps_min = material0.get_index(wavelength_target) ** 2
material1 = db.materials["main"]["Si"]["Aspnes"]
eps_max = material1.get_index(wavelength_target) ** 2


################################################################
# Geometry parameters

R = wavelength_target * 0.5
Rcalc = R + 1 * wavelength_target
lbox = Rcalc * 2 * 1.2
lmin = 0.4 / pmesh
ref_des = 2 * abs(eps_max.real) ** 0.5

################################################################
# Optimization parameters

filtering_type = "density"
# filtering_type = "sensitivity"
rfilt = R / 5
maxiter = 40
threshold = (0, 8)

################################################################
# Class for geometry

class Scatterer:
    "The geometry"

    def __init__(self, R, Rcalc, lbox, lmin, ref_des):
        self.R = R
        self.Rcalc = Rcalc
        self.lbox = lbox
        self.lmin = lmin
        self.lmin = ref_des

        geom = gy.BoxPML(
            dim=2,
            box_size=(lbox, lbox),
            pml_width=(0.8, 0.8),
            Rcalc=Rcalc,
        )
        cyl = geom.add_circle(0, 0, 0, R)
        cyl, *box = geom.fragment(cyl, geom.box)
        geom.add_physical(box, "box")
        geom.add_physical(cyl, "design")
        des_bnds = geom.get_boundaries("design")
        geom.add_physical(des_bnds, "des_bnds",dim=1)
        geom.set_pml_mesh_size(lmin * 0.7)
        geom.set_size("box", lmin)
        geom.set_size("design", lmin / ref_des)
        geom.remove_all_duplicates()
        self.geometry = geom

    def build(self):
        self.geometry.build()


################################################################
# Function to filter and project density


def density_proj_filt(self, density, proj, filt, filtering_type):
    if filtering_type == "density":
        density_f = self.filter.apply(density) if filt else density
    else:
        density_f = density
    density_fp = (
        go.projection(density_f, beta=2**self.proj_level) if proj else density_f
    )
    fs_plot = gy.dolfin.FunctionSpace(self.submesh, "DG", 0)
    return gu.project_iterative(density_fp, fs_plot)


################################################################
# Define callback function

jopt = 0


def callback(self):
    global jopt
    proj = self.proj_level is not None
    filt = self.filter != 0
    density_fp = density_proj_filt(self, self.density, proj, filt, filtering_type)
    plt.clf()
    gy.plot(density_fp, ax=plt.gca(), vmin=0, vmax=1, cmap="Reds")
    plt.xlim(-R, R)
    plt.ylim(-R, R)
    plt.axis("off")
    plt.title(f"iteration {jopt}, objective = {-self.objective:.5f}")
    # plt.tight_layout()
    gy.pause(0.1)
    plt.savefig(f"{datadir}/density_{str(jopt).zfill(4)}.png")
    jopt += 1
    return self.objective


################################################################
# The main simulation class


class Simulation:
    """The simulation"""

    def __init__(self, scatterer, wavelength, polarization, epsilon_design=1):
        self.scatterer = scatterer
        self._wavelength = wavelength
        self.polarization = polarization
        self.geometry = scatterer.geometry
        self._epsilon_design = epsilon_design
        self.mu = dict(box=1, design=1)
        self.optimizer = go.TopologyOptimizer(
            self.objective_function,
            self.geometry,
            eps_bounds=(eps_min, eps_max),
            rfilt=rfilt,
            filtering_type=filtering_type,
            callback=callback,
            verbose=True,
            ftol_rel=1e-6,
            xtol_rel=1e-12,
            maxiter=maxiter,
            threshold=threshold,
        )

    @property
    def epsilon_design(self):
        return self._epsilon_design

    @epsilon_design.setter
    def epsilon_design(self, val):
        self._epsilon_design = val

    @property
    def wavelength(self):
        return self._wavelength

    @wavelength.setter
    def wavelength(self, val):
        self._wavelength = val

    @property
    def epsilon(self):
        return dict(box=1, design=self.epsilon_design)

    @property
    def planewave(self):
        return gy.PlaneWave(
            wavelength=self.wavelength,
            angle=gy.pi / 2,
            dim=2,
            domain=self.geometry.mesh,
            degree=degree,
        )

    def solve(self):
        self.simulation = gy.Scattering(
            self.geometry,
            self.epsilon,
            self.mu,
            source=self.planewave,
            degree=degree,
            polarization=self.polarization,
        )
        self.simulation.solve()
        self.solution = self.simulation.solution


    def eigensolve(self,*args,**kwargs):
        self.simulation = gy.Scattering(
            self.geometry,
            self.epsilon,
            self.mu,
            source=None,
            degree=degree,
            polarization=self.polarization,
            modal=True,
        )
        self.simulation.eigensolve(*args,**kwargs)
        self.solution = self.simulation.solution

    # define objective function
    def objective_function(self, epsilon_design):
        self.epsilon_design = epsilon_design
        self.solve()
        self.SCS = self.simulation.scattering_cross_section()
        return -self.SCS / (2 * self.scatterer.R)

    def optimize(self, x0):
        # define optimizer

        if np.isscalar(x0):
            x0 *= np.ones(self.optimizer.nvar)
        # optimize
        self.optimizer.minimize(x0)

    def binarize(self, xopt):
        opt = self.optimizer
        density = gu.array2function(xopt, opt.fs_sub)
        density_fp = density_proj_filt(opt, density, True, True, filtering_type)
        density_fp_array = gu.function2array(density_fp)
        density_bin = density_fp_array.copy()
        density_bin[density_fp_array < 0.5] = 0
        density_bin[density_fp_array >= 0.5] = 1
        density_bin = gu.array2function(density_bin, opt.fs_sub)
        return density_bin

    def simp(self, dens, eps_min=None, eps_max=None, p=None):
        opt = self.optimizer
        eps_min = eps_min or opt.eps_min
        eps_max = eps_max or opt.eps_max
        p = p or opt.p
        return go.simp(dens, gy.Constant(eps_min), gy.Constant(eps_max), p) * gy.Complex(1, 0)



################################################################
# Main function


def main():

    # Initialize geometry and simulation

    scat = Scatterer(R, Rcalc, lbox, lmin, ref_des)
    scat.build()
    simu = Simulation(scat, wavelength_target, polarization)

    plt.figure(figsize=(4, 3.5))

    # optimize
    simu.optimize(x0=0.5)

    opt = simu.optimizer
    # binary design
    density_bin = simu.binarize(opt.xopt)
    epsilon_design = simu.simp(density_bin)
    # final objective
    obj_final = simu.objective_function(epsilon_design)

    # save data
    np.savez(
        f"{datadir}/opt_scatt.npz",
        xopt=opt.xopt,
        density_bin=gu.function2array(density_bin),
        obj_final=obj_final,
        history=opt.callback_output,
    )



def compute_spectrum(polarization):

    # Initialize geometry and simulation

    scat = Scatterer(R, Rcalc, lbox, lmin, ref_des)
    scat.build()
    simu = Simulation(scat, wavelength_target, polarization)

    opt = simu.optimizer
    opt.proj_level=7
    # binary design

    arch = np.load(f"data/gyptis/{polarization}/opt_scatt.npz", allow_pickle=True)
    xopt = arch["xopt"]
    density_bin = simu.binarize(xopt)
    epsilon_design = simu.simp(density_bin)

    # wavelength spectrum

    @gu.adaptive_sampler()
    def cs_vs_wl(wavelength):
        eps_min = material0.get_index(wavelength) ** 2
        eps_max = material1.get_index(wavelength) ** 2
        epsilon_design = simu.simp(density_bin, eps_min, eps_max)
        simu.wavelength = wavelength
        scsn = -simu.objective_function(epsilon_design)
        print(f"wavelength = {wavelength:.3f}, SCS norma = {scsn:.3f}")
        return scsn

    wl = np.linspace(0.4, 0.8, 201)
    wl, scs_norma = cs_vs_wl(wl)

    # save data
    np.savez(
        f"{datadir}/opt_scatt_spectrum.npz",
        wl=wl,
        scs_norma=scs_norma,
    )


################################################################
#

if __name__ == "__main__":
    main()
