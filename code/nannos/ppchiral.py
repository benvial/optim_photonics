#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# License: MIT
"""
Postprocessing
===============

"""

from chiral import *
nn.set_backend("torch")

plt.close("all")
plot3D = False

build_anim = False



os.makedirs(f"figs", exist_ok=True)

if build_anim:
    os.system(f"convert -delay 20 -loop 0 {datadir}/ms*.png figs/animation_nannos.gif")
    os.system(f"convert figs/animation_nannos.gif figs/animation_nannos.mp4")
    os.system(f"rm -f figs/animation_nannos.gif")


arch = np.load(f"{datadir}/opt_order.npz", allow_pickle=True)


density_bin = arch["density_bin"]
obj_final = arch["obj_final"]
historyTE = arch["historyTE"]
historyTM = arch["historyTM"]
T_opt = arch["T_opt"].tolist()
T_opt0 = arch["T_opt0"].tolist()
WL_opt = arch["WL_opt"].tolist()
WL_opt0 = arch["WL_opt0"].tolist()


if plot3D:

    #### plot with pyvista: requires good hardware
    bk = nn.backend
    density_bin = bk.array(density_bin)
    density = bk.reshape(density_bin, (Nx, Ny))
    # density_bin = symmetrize(density_bin)
    # density = density[::2,::2]
    # Nx = 2**8
    # Ny = 2**8
    nn.set_backend("numpy")
    bk = nn.backend

    epsgrid = no.simp(density_bin, eps_min, eps_max, p=1)
    pw = nn.PlaneWave(wavelength=1, angles=(0, 0, 0))
    sup = lattice.Layer("Superstrate", epsilon=eps_sup)
    sub = lattice.Layer("Substrate", epsilon=eps_sub)
    ms = lattice.Layer("Metasurface", epsilon=1, thickness=h_ms)
    ms.epsilon = epsgrid
    stack = [sup, ms, sub]
    sim = nn.Simulation(stack, pw, nh, formulation=formulation)
    layer_colors = ["#f4dcc6", [None,"#91b995"], None]
    layer_metallic = [0.3, [None,0.3], None]
    layer_roughness = [0.3, [None,0.1], None]
    plotter = sim.plot_structure(
        null_thickness=h_ms,
        nper=(5, 10),
        layer_colors=layer_colors,
        layer_metallic=layer_metallic,
        layer_roughness=layer_roughness,
    )
    plotter.camera.azimuth = 15
    plotter.camera.elevation = -222
    plotter.camera.roll = 70
    # xsx
    plotter.camera.zoom(2)
    plotter.hide_axes()
    plotter.show(screenshot="figs/metasurface.png")
    # plotter.show(screenshot=f"structure.png")



fig, ax = plt.subplots(1, 2, figsize=(10, 4.3))
# ax[1].set_title("init")
# ax[0].set_ylim(0, 1)
# ax[0].set_xlim(0,maxiter*8)

ax[0].set_xlabel("iteration")
ax[0].set_ylabel(f"$T_{{{order_target[0]},{order_target[1]}}}$")

tte = historyTE[-1]
ttm = historyTM[-1]

ax[0].set_title(f"objective = {obj_final:.3f}")
plt.tight_layout()

plt.sca(ax[1])
imshow(density_bin, vmin=0, vmax=1, cmap="Greens")

cbar = plt.colorbar(orientation="horizontal", ax=ax[1])
ax[1].set_axis_off()
ax[1].set_title(f"density")
# plt.tight_layout()

ax[0].plot(bk.array(historyTE).real, "-", ms=2, mew=0, c="#d13526", label="TE", alpha=0.4)
ax[0].plot(bk.array(historyTM).real, "-", ms=2, mew=0, c="#266fd1", label="TM", alpha=0.4)

# ax[0].set_xlim(0,maxiter*8)
# ax[0].set_ylim(0, 1)
ax[0].set_xlabel("iteration")
ax[0].set_ylabel(f"$T_{{{order_target[0]},{order_target[1]}}}$")
ax[0].legend(loc="lower right")

plt.show()

plt.tight_layout()
ax[1].annotate(
    "(b)", (-0.05, 1.05), xycoords="axes fraction", fontsize=8, fontweight="400"
)

ax[0].annotate(
    "(a)", (-0.16, 1.05), xycoords="axes fraction", fontsize=8, fontweight="400"
)


plt.pause(0.1)

plt.savefig(f"figs/figure3.eps")
plt.savefig(f"figs/figure3.png")

nn.set_backend("torch")
bk = nn.backend


fig, ax = plt.subplots(1)
for polarization in ["TE", "TM"]:
    color = "#6e8cd0" if polarization == "TE" else "#d0716e"
    # plt.plot(
    #     WL_opt0[polarization] * 1000,
    #     T_opt0[polarization],
    #     "--",
    #     c=color,
    #     label=polarization + " init",
    # )
    plt.plot(
        WL_opt[polarization] * 1000,
        T_opt[polarization],
        c=color,
        label="T " +polarization ,
    )
plt.xlabel("wavelength (nm)")
# plt.legend()
# ax.set_ylabel(f"$T_{{{order_target[0]},{order_target[1]}}}$")


plt.tight_layout()


ax.vlines(wl_target * 1000, -1, 1, ls="--", color="#a4a4a4")
# ax.set_ylim(0, 1)
ax.set_xlim(655, 755)


for psi in [0, 90]:
    nn.set_backend("torch")
    density_bin = bk.array(density_bin)
    sim = initialize_simulation(density_bin, None, 0, wl=wl_target, psi=psi, nh=nh)


    E = sim.get_Efield_grid(1, z=h_ms).numpy()[:,:,:,0]

    H = sim.get_Hfield_grid(1, z=h_ms).numpy()[:,:,:,0]



    nn.set_backend("numpy")
    # normE2 = np.sum(np.abs(E) ** 2, axis=0)[:,:,0]

    poynting = 0.5*(np.cross(E,H,axisa=0,axisb=0)).real
    normP2 = np.sum(poynting ** 2, axis=-1)**0.5
    # normP2 = np.sum(poynting[:,:,:2] ** 2, axis=-1)

    x,y = lattice.grid
    # ,extent=(0,L1[0],0,L2[1])

    polarization = "TE" if psi == 0 else "TM"

    if polarization == "TE":
        inset = fig.add_axes([0.07, 0.68, 0.45, 0.22])
    else:
        inset = fig.add_axes([0.5, 0.24, 0.45, 0.22])


    color = "#6e8cd0" if polarization == "TE" else "#d0716e"



    np2 = inset.pcolormesh(x,y,normP2,cmap="inferno")
    ds = 2**4
    Px = poynting[::ds,::ds,0]
    Py = poynting[::ds,::ds,1]
    inset.quiver(x[::ds,::ds],y[::ds,::ds],Px,Py,color="w")
    inset.pcolormesh(x,y,density_bin,cmap="Greys",alpha=0.2)
    inset.set_axis_off()
    plt.axis("scaled")
    cbar = plt.colorbar(np2,ax=inset)
    ticklabs = cbar.ax.get_yticklabels()
    # cbar.ax.set_yticklabels(ticklabs, fontsize=4)
    # cbar.ax.set_yticklabels(ticklabs, fontsize=6)
    for t in cbar.ax.get_yticklabels():
         t.set_fontsize(8)
    inset.set_title(polarization,pad=0.1,loc="left",c=color)
    # plt.tight_layout()




plt.savefig(f"figs/figure4.eps")
plt.savefig(f"figs/figure4.png")




fig, ax = plt.subplots(1)
Nwl = max(len(T_opt["TM"]),len(T_opt["TE"]))*5
wlopt = WL_opt["TE"]
wls = np.linspace(wlopt[0],wlopt[-1],Nwl)
tyy =np.interp(wls,WL_opt["TM"],np.array(T_opt["TM"])[:,0])
txx =np.interp(wls,WL_opt["TE"],np.array(T_opt["TE"])[:,0])
CE = np.abs((tyy-txx)/2)**2
plt.plot(
    wls * 1000,
    CE,
    c="#43b173",
)
plt.xlabel("wavelength (nm)")
# plt.legend()
ax.set_ylabel(f"CE")



ax.vlines(wl_target * 1000, 0, 1, ls="--", color="#a4a4a4")
ax.set_ylim(0, 1)
ax.set_xlim(655, 755)

plt.tight_layout()
