#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# License: MIT

"""
Topology optimization
=====================

Design of a metasurface with maximum transmission into a given order for both polarizations.
"""


import os
import argparse
import matplotlib.pyplot as plt
import numpy as np
import refidx as ri
import nannos as nn


## matplotlib config
plt.close("all")
plt.ion()

################################################################
# Create data directory
datadir = "data/nannos"
os.makedirs(datadir, exist_ok=True)

################################################################
# Funtion to save figures
def savefig(base, it):
    name = datadir + "/" + base + str(it).zfill(4) + ".png"
    plt.savefig(name)


################################################################
# Parse arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    "-i", "--init", default="post", help="initial density (random, mid, post or hole)"
)
args = parser.parse_args()
init = args.init

################################################################
# Set a backend supporting automatic differentiation
nn.set_backend("torch")
no = nn.optimize
bk = nn.backend


################################################################
# RCWA parameters
formulation = "original"
nh = 200
L1 = [0.8, 0]
L2 = [0, 0.4]
Nx = 2**9
Ny = 2**9
h_ms = 0.35
lattice = nn.Lattice((L1, L2), discretization=(Nx, Ny))

################################################################
# Optimization parameters
# rfilt = Nx / 50
rfilt = Nx / 40, L1[0] / L2[1] * Ny / 40
maxiter = 40
threshold = (0, 8)
order_target = (1, 0)
wl_target = 0.732
alpha = 0.5

################################################################
# Materials
db = ri.DataBase()
material_sup = db.materials["main"]["SiO2"]["Lemarchand"]
index_sup = material_sup.get_index(wl_target)
eps_sup = index_sup.real**2
eps_sub = 1.0
eps_min = 1.0
material = db.materials["main"]["Si"]["Aspnes"]
index = material.get_index(wl_target)
eps_max = index.conj() ** 2
# eps_max = index.real ** 2

################################################################
# Force symmetry
def symmetrize(dens, x=False, y=True):
    if y == True:
        dens = 0.5 * (dens + bk.fliplr(dens))
    if x == True:
        dens = 0.5 * (dens + bk.flipud(dens))
    return dens


################################################################
# Function to initialize a simulation


def initialize_simulation(
    density,
    proj_level=None,
    rfilt=0,
    wl=1,
    nh=nh,
    psi=0,
    eps_max=eps_max,
    eps_sup=eps_sup,
):
    density = bk.reshape(density, (Nx, Ny))
    density = symmetrize(density)
    density_f = no.apply_filter(density, rfilt)
    density_fp = (
        no.project(density_f, proj_level) if proj_level is not None else density_f
    )
    epsgrid = no.simp(density_fp, eps_min, eps_max, p=1)
    pw = nn.PlaneWave(wavelength=wl, angles=(0, 0, psi))
    sup = lattice.Layer("Superstrate", epsilon=eps_sup)
    sub = lattice.Layer("Substrate", epsilon=eps_sub)
    ms = lattice.Layer("Metasurface", epsilon=1, thickness=h_ms)
    ms.epsilon = epsgrid
    stack = [sup, ms, sub]
    sim = nn.Simulation(stack, pw, nh, formulation=formulation)
    return sim


##############################################################################
# Define objective function


def fun(density, proj_level, rfilt, nh=nh, alpha=alpha):
    global tte
    global ttm

    sim_TM = initialize_simulation(
        density, proj_level, rfilt, wl=wl_target, nh=nh, psi=90
    )
    _, T_TM = sim_TM.diffraction_efficiencies(orders=True)
    ttm = sim_TM.get_order(T_TM, order_target)

    sim_TE = initialize_simulation(
        density, proj_level, rfilt, wl=wl_target, nh=nh, psi=0
    )
    _, T_TE = sim_TE.diffraction_efficiencies(orders=True)
    tte = sim_TE.get_order(T_TE, order_target)

    objective = -(alpha * tte + (1 - alpha) * ttm)
    print("T TE = ", float(tte))
    print("T TM = ", float(ttm))
    # print("objective = ", objective)
    return objective
    # return -tte


##############################################################################
# Define initial density


def initialize_density(init):

    r0 = 0.3

    if init == "post":
        density0 = lattice.ones() * 0
        circ = lattice.circle((0.5 * L1[0], 0.5 * L2[1]), r0 * L2[1])
        density0[circ] = 1

        # circ = lattice.circle((0.33 * L1[0], 0.5 * L2[1]), r0 * L2[1])
        # density0[circ] = 1
        # circ = lattice.circle((0.66 * L1[0], 0.5 * L2[1]), r0 * L2[1])
        # density0[circ] = 1
        density0 = density0.real
        # density0 = 0.5+0.1*density0
    elif init == "hole":
        density0 = lattice.ones()
        circ = lattice.circle((0.5 * L1[0], 0.5 * L2[1]), r0 * L2[1])
        density0[circ] = 0
        density0 = density0.real
    elif init == "random":
        # np.random.seed(13)
        density0 = np.random.rand(Nx, Ny)
        density0 = bk.array(density0)
        density0 = symmetrize(density0)
        rfilt0 = rfilt
        density0 = no.apply_filter(density0, rfilt0)
        density0 = (density0 - density0.min()) / (density0.max() - density0.min())
    elif init == "mid":
        density0 = np.random.rand(Nx, Ny)
        density0 = bk.array(density0)
        density0 = 0.5 + density0 * 1e-12
        density0 = symmetrize(density0)
        density0 = no.apply_filter(density0, rfilt)
    else:
        raise ValueError

    return density0


##############################################################################
# Function to plot density
def imshow(s, *args, **kwargs):
    extent = (0, L1[0] / L2[1], 0, 1)
    if nn.DEVICE == "cuda":
        plt.imshow(s.T.cpu(), *args, extent=extent, **kwargs)
    else:
        plt.imshow(s.T, *args, extent=extent, **kwargs)


##############################################################################
# Define calback function

TTE = []
TTM = []
it = 0
fig, ax = plt.subplots(1, 2, figsize=(6, 2.3))


def callback(x, y, proj_level, rfilt, nh, alpha):
    global it
    global cbar
    global TTE
    global TTM
    print(f"iteration {it}")
    TTE.append(tte)
    TTM.append(ttm)
    density = bk.reshape(x, (Nx, Ny))
    density = symmetrize(density)
    density_f = no.apply_filter(density, rfilt)
    density_fp = no.project(density_f, proj_level)
    # plt.figure()
    # plt.clf()
    try:
        cbar.remove()
    except:
        pass
    ax[1].clear()
    plt.sca(ax[1])
    imshow(density_fp, vmin=0, vmax=1, cmap="Greens")

    cbar = plt.colorbar(orientation="horizontal", ax=ax[1])

    ax[1].set_axis_off()
    ax[1].set_title(f"iteration {it}, objective = {-y:.5f}")

    # plt.tight_layout()

    ax[0].clear()
    ax[0].plot(range(it + 1), TTE, "-", c="#d13526", label="TE", alpha=0.24)
    ax[0].plot(range(it + 1), TTM, "-", c="#266fd1", label="TM", alpha=0.24)
    ax[0].plot(it, tte, "o", c="#d13526")
    ax[0].plot(it, ttm, "o", c="#266fd1")
    ax[0].plot(
        range(it + 1),
        (1 - alpha) * np.array(TTM) + alpha * np.array(TTE),
        "-",
        c="#222222",
        label="mean",
    )
    ax[0].plot(it, (1 - alpha) * ttm + alpha * tte, "o", c="#222222")
    ax[0].set_xlim(0, maxiter * (threshold[-1]) + 10)
    ax[0].set_ylim(0, 1)
    ax[0].set_xlabel("iteration")
    ax[0].set_ylabel(f"$T_{{{order_target[0]},{order_target[1]}}}$")
    ax[0].set_title(f"TE = {100*tte:.1f}%, TM = {100*ttm:.1f}%")
    ax[0].legend(loc="lower right")
    plt.tight_layout()
    plt.pause(0.1)
    savefig("ms", it)
    it += 1


# callback = None

##############################################################################
# Optimization function
def optimize(
    fun, density0, nh, threshold=(0, 8), maxiter=40, callback=callback, alpha=alpha
):
    opt = no.TopologyOptimizer(
        fun,
        density0,
        method="nlopt",
        threshold=threshold,
        maxiter=maxiter,
        stopval=-0.99,
        args=(1, rfilt, nh, alpha),
        callback=callback,
        verbose=True,
        options={},
    )
    density_opt, f_opt = opt.minimize()
    return opt, density_opt, f_opt


################################################################
# Main function


def main():
    # initialize density
    density0 = initialize_density(init)
    density0 = density0.flatten()

    # optimize
    opt, density_opt, f_opt = optimize(
        fun,
        density0,
        nh=nh,
        threshold=threshold,
        maxiter=maxiter,
        callback=callback,
        alpha=alpha,
    )
    # binary design
    density_opt = bk.reshape(bk.array(density_opt), (Nx, Ny))
    density_opt = symmetrize(density_opt)
    density_optf = no.apply_filter(density_opt, rfilt)
    proj_level = 2 ** (opt.threshold[-1] - 1)
    density_optfp = no.project(density_optf, proj_level)
    density_bin = bk.ones_like(density_optfp)
    density_bin[density_optfp < 0.5] = 0

    # final objective
    obj_final = 0
    for psi in [0, 90]:
        sim = initialize_simulation(density_bin, None, 0, wl=wl_target, psi=psi, nh=nh)
        R, T = sim.diffraction_efficiencies(orders=True)
        Ttarget = sim.get_order(T, order_target)
        print("")
        print(f"Target transmission in order {order_target}")
        print(f"===================================")
        print(f"T_{order_target} = ", float(Ttarget))
        obj_final += Ttarget
        if psi == 0:
            TTE.append(Ttarget)
        else:
            TTM.append(Ttarget)

    obj_final /= 2

    # wavelength spectrum
    wl = np.linspace(0.655, 0.755, 101)

    def t_vs_wl(wavelength, density, psi):
        print(wavelength)
        index_sup = material_sup.get_index(wavelength)
        eps_sup = index_sup.real**2
        index = material.get_index(wavelength)
        eps_max = index.conj() ** 2
        sim = initialize_simulation(
            density,
            None,
            0,
            wl=wavelength,
            psi=psi,
            nh=nh,
            eps_max=eps_max,
            eps_sup=eps_sup,
        )
        R, T = sim.diffraction_efficiencies(orders=True)
        Ttarget = sim.get_order(T, order_target)
        return np.array([Ttarget.numpy()])

    def compute_spectrum(density):
        T_opt = dict()
        WL_opt = dict()
        for psi in [0, 90]:
            polarization = "TE" if psi == 0 else "TM"

            def f(wl):
                return t_vs_wl(wl, density, psi)

            wlopt, t = nn.adaptive_sampler(f, wl)
            T_opt[polarization] = t
            WL_opt[polarization] = wlopt
        return WL_opt, T_opt

    WL_opt, T_opt = compute_spectrum(density_bin)
    WL_opt0, T_opt0 = compute_spectrum(density0)

    # save data
    np.savez(
        f"{datadir}/opt_order.npz",
        density_bin=density_bin,
        obj_final=obj_final,
        historyTE=TTE,
        historyTM=TTM,
        T_opt=T_opt,
        T_opt0=T_opt0,
        WL_opt=WL_opt,
        WL_opt0=WL_opt0,
    )


################################################################
#

if __name__ == "__main__":
    main()
