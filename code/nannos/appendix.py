#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial

import matplotlib.pyplot as plt
import numpy as np

import nannos as nn
import os

os.makedirs(f"figs", exist_ok=True)

np.set_printoptions(precision=3)

plt.ion()
plt.close("all")


def checkerboard(nh, formulation):
    la = 1
    a = 1.25 * la
    d = a * 2**0.5
    Nx = 1000
    Ny = 1000
    lattice = nn.Lattice(([d, 0], [0, d]), discretization=(Nx, Ny))
    pw = nn.PlaneWave(wavelength=la, angles=(0, 0, -45))
    epsgrid = lattice.ones() * 2.25
    sq = lattice.square((0.5 * d, 0.5 * d), a, rotate=45)
    epsgrid[sq] = 1

    sup = lattice.Layer("Superstrate", epsilon=2.25)
    sub = lattice.Layer("Substrate", epsilon=1)
    st = lattice.Layer("Structured", la)
    st.epsilon = epsgrid

    sim = nn.Simulation([sup, st, sub], pw, nh, formulation=formulation)
    R, T = sim.diffraction_efficiencies(orders=True)
    return R, T, sim


R, T, sim = checkerboard(12, "tangent")

Tnannos = np.zeros((3, 3))
for i1, i in enumerate(range(-1, 2)):
    for j1, j in enumerate(range(-1, 2)):
        Tnannos[i1, j1] = sim.get_order(T, (i, j)) * 100

Tnannos[Tnannos < 1e-8] = 0
print("Checkerboard grating")
print("###########################")
print("number of harmonics:", sim.nh)
print("discretization:", sim.lattice.discretization)
print("truncation method:", sim.lattice.truncation)
print("formulation:", sim.formulation)
print(" ")
print("Transmission (%)")
print(Tnannos)
print(" ")


NH = [50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600]
formulations = ["original", "tangent"]
nhs = {f: [] for f in formulations}
ts = {f: [] for f in formulations}

markers = {"original": "^", "tangent": "o"}
colors = {
    "original": "#3395d4",
    "tangent": "#54aa71",
}


plt.figure()
plt.xlabel("number of Fourier harmonics $n_h$")
plt.ylabel("$T_{0,-1}$")
plt.ylim(0.1255, 0.129)
plt.tight_layout()


for nh in NH:
    print("============================")
    print("number of harmonics = ", nh)
    print("============================")
    

    for formulation in formulations:
        Ri, Ti, sim = checkerboard(nh, formulation=formulation)
        R = np.sum(Ri)
        T = np.sum(Ti)
        t = sim.get_order(Ti, (0, -1))
        print("formulation = ", formulation)
        print("nh0 = ", nh)
        print("nh = ", sim.nh)
        print("t = ", t)
        print("R = ", R)
        print("T = ", T)
        print("R+T = ", R + T)
        print("-----------------")
        nhs[formulation].append(sim.nh)
        ts[formulation].append(t)

        
        plt.plot(
            nhs[formulation],
            ts[formulation],
            "-",
            color=colors[formulation],
            marker=markers[formulation],
            label=formulation,
        )
        plt.pause(0.1)
    
    if nh==50:
        plt.legend()

#########################################################################
# Plot the results:

plt.figure()

for formulation in formulations:
    plt.plot(
        nhs[formulation],
        ts[formulation],
        "-",
        color=colors[formulation],
        marker=markers[formulation],
        label=formulation,
    )
    plt.pause(0.1)
plt.legend()
plt.xlabel("number of Fourier harmonics $n_h$")
plt.ylabel("$T_{0,-1}$")
plt.ylim(0.1254, 0.129)
plt.tight_layout()


plt.savefig(f"figs/figureA2.eps")
plt.savefig(f"figs/figureA2.png")
