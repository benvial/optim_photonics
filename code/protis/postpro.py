#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# License: MIT

"""
Postprocessing
===============

"""

from phc import *

os.makedirs("figs", exist_ok=True)
os.system(f"convert -delay 20 -loop 0 {datadir}/bd_bandgap_TE*.png figs/animation_protis_bandgap_TE.gif")
os.system(f"convert figs/animation_protis_bandgap_TE.gif figs/animation_protis_bandgap_TE.mp4")
os.system(f"convert -delay 20 -loop 0 {datadir}/bd_dispersion_TM*.png figs/animation_protis_dispersion_TM.gif")
os.system(f"convert figs/animation_protis_dispersion_TM.gif figs/animation_protis_dispersion_TM.mp4")
os.system(f"rm -f figs/animation_protis_bandgap_TE.gif")
os.system(f"rm -f figs/animation_protis_dispersion_TM.gif")


fig_bg, ax_bg = plt.subplots(1,2, figsize=(6., 3.))

plt.sca(ax_bg[0])
ax_ins = inset_axes(
    ax_bg[0],
    width="100%",
    height="100%",
    bbox_to_anchor=(0.35, 0.01, 0.2, 0.2),
    bbox_transform=ax_bg[0].transAxes,
    loc=8,
)
arch = np.load(f"{datadir}/bd_opt_bandgap_TE.npz",allow_pickle=True)
band_diag=arch["band_diag"]
band_diag_approx=arch["band_diag_approx"]
obj_approx=arch["obj_approx"]
obj=arch["obj"]
density_bin=arch["density_bin"]

plot_bd(band_diag, density_bin, ax_bg[0], ax_ins,"TE",type_opt="bandgap",ieig=4)
# plot_bands = ax_bg.plot(bands_plot, band_diag_approx, "--", c=color, lw=2, alpha=0.3)

plt.pause(0.1)

arch = np.load(f"{datadir}/bd_opt_dispersion_TM.npz",allow_pickle=True)
band_diag=arch["band_diag"]
band_diag_approx=arch["band_diag_approx"]
obj_approx=arch["obj_approx"]
obj=arch["obj"]
density_bin=arch["density_bin"]

plt.sca(ax_bg[1])
ax_ins = inset_axes(
    ax_bg[1],
    width="100%",
    height="100%",
    bbox_to_anchor=(0.35, 0.01, 0.2, 0.2),
    bbox_transform=ax_bg[1].transAxes,
    loc=8,
)

plot_bd(band_diag, density_bin, ax_bg[1], ax_ins,"TM",type_opt="dispersion",ieig=5)
# plot_bands = ax_bg.plot(bands_plot, band_diag_approx, "--", c=color, lw=2, alpha=0.3)

plt.pause(0.1)


ax_bg[0].annotate(
    "(a)", (-0.15, 1.05), xycoords="axes fraction", fontsize=8, fontweight="400"
)
ax_bg[1].annotate(
    "(b)", (-0.15, 1.05), xycoords="axes fraction", fontsize=8, fontweight="400"
)

plt.savefig(f"figs/figure5.eps")
plt.savefig(f"figs/figure5.png")
