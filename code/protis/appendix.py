#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# This file is part of protis
# License: GPLv3
# See the documentation at protis.gitlab.io


"""
Reduced Bloch mode expansion
=============================

Calculation of the band diagram of a two-dimensional photonic crystal.
"""


import os
import matplotlib.pyplot as plt
import numpy as np

from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import protis as pt

plt.ion()
##############################################################################
# Reference results are taken from :cite:p:`Hussein2009`.

a = 1
lattice = pt.Lattice([[a, 0], [0, a]], discretization=2**9)


##############################################################################
# Define the permittivity
epsilon = lattice.ones() * 1
column = lattice.circle((0.5*a, 0.5*a), 0.2*a)
epsilon[column] = 8.9
mu = 1

##############################################################################
# We define here the wavevector path:


def k_space_path(Nb):
    K = np.linspace(0, np.pi / a, Nb)
    bands = np.zeros((3 * Nb - 3, 2))
    bands[:Nb, 0] = K
    bands[Nb : 2 * Nb, 0] = K[-1]
    bands[Nb : 2 * Nb - 1, 1] = K[1:]
    bands[2 * Nb - 1 : 3 * Nb - 3, 0] = bands[2 * Nb - 1 : 3 * Nb - 3, 1] = np.flipud(
        K
    )[1:-1]
    return bands, K


##############################################################################
# Full model:


def full_model(bands, nh=100):
    t0 = pt.tic()
    sim = pt.Simulation(lattice, epsilon=epsilon, mu=mu, nh=nh)
    BD = {}
    for polarization in ["TE", "TM"]:
        ev_band = []
        for kx, ky in bands:
            sim.k = kx, ky
            sim.solve(polarization, vectors=False)
            ev_norma = sim.eigenvalues * a / (2 * np.pi)
            ev_band.append(ev_norma)
        # append first value since this is the same point
        ev_band.append(ev_band[0])
        BD[polarization] = ev_band
    BD["TM"] = pt.backend.stack(BD["TM"]).real
    BD["TE"] = pt.backend.stack(BD["TE"]).real
    t_full = pt.toc(t0, verbose=False)
    return BD, t_full, sim


##############################################################################
# Reduced Bloch mode expansion


def rbme_model(bands, nh=100, Nmodel=2, N_RBME=8):
    t0 = pt.tic()
    sim = pt.Simulation(lattice, epsilon=epsilon, mu=mu, nh=nh)
    q = pt.pi / a
    if Nmodel == 2:
        bands_RBME = [(0, 0), (q, 0), (q, q)]
    elif Nmodel == 3:
        bands_RBME = [(0, 0), (q / 2, 0), (q, 0), (q, q / 2), (q, q), (q / 2, q / 2)]
    else:
        raise ValueError
    rbme = {}
    for polarization in ["TE", "TM"]:
        rbme[polarization] = sim.get_rbme_matrix(N_RBME, bands_RBME, polarization)

    BD_RBME = {}
    for polarization in ["TE", "TM"]:
        ev_band = []
        for kx, ky in bands:
            sim.k = kx, ky
            sim.solve(polarization, vectors=False, rbme=rbme[polarization])
            ev_norma = sim.eigenvalues * a / (2 * np.pi)
            ev_band.append(ev_norma)
        # append first value since this is the same point
        ev_band.append(ev_band[0])
        BD_RBME[polarization] = ev_band
    BD_RBME["TM"] = pt.backend.stack(BD_RBME["TM"]).real
    BD_RBME["TE"] = pt.backend.stack(BD_RBME["TE"]).real
    t_rbme = pt.toc(t0, verbose=False)
    return BD_RBME, t_rbme, sim


bands, K = k_space_path(Nb=49)
BD, t_full, sim_full = full_model(bands, nh=400)
BD_RBME, t_rbme, sim_rbme = rbme_model(bands, nh=400, Nmodel=2, N_RBME=8)
print(f"speedup = {t_full/t_rbme}")

##############################################################################
# Plot the bands:


def k_space_path_plot(Nb, K):
    bands_plot = np.zeros(3 * Nb - 2)
    bands_plot[:Nb] = K
    bands_plot[Nb : 2 * Nb - 1] = K[-1] + K[1:]
    bands_plot[2 * Nb - 1 : 3 * Nb - 2] = 2 * K[-1] + 2**0.5 * K[1:]
    return bands_plot


bands_plot = k_space_path_plot(49, K)
def plot_bz(ax,color):
    axins = inset_axes(
        ax,
        width="100%",
        height="100%",
        loc=4,
        bbox_to_anchor=(0.6, 0.0, 0.15, 0.15),
        bbox_transform=ax.transAxes,
    )


    axins.plot([-1,1],[-1,-1],"k")
    axins.plot([-1,1],[1,1],"k")
    axins.plot([1,1],[-1,1],"k")
    axins.plot([-1,-1],[-1,1],"k")
    axins.plot([0,1],[0,1],"k")
    axins.plot([-1,1],[0,0],"k")
    axins.plot([0,0],[-1,1],"k")
    axins.fill_between([0,1],[0,1],color=color,linewidth=0,alpha=0.4)
    axins.annotate(
        "$\Gamma$", (0.35, 0.33), xycoords="axes fraction", fontsize=4
    )
    axins.annotate(
        "$X$", (1.01, 0.33), xycoords="axes fraction", fontsize=4
    )
    axins.annotate(
        "$M$", (1.01, 1.01), xycoords="axes fraction", fontsize=4
    )

    axins.set_axis_off()
    plt.axis("scaled")


##############################################################################
# TE polarization:



fig,ax = plt.subplots(1,2,figsize=(6.,2.5))
plt.sca(ax[0])
plotTE = plt.plot(bands_plot, BD["TE"], c="#cf5268", lw=1.5, alpha=0.5)
plotTE_RBME = plt.plot(bands_plot, BD_RBME["TE"], "--", c="#cf5268")
plt.ylim(0, 1.2)
plt.xlim(0, bands_plot[-1])
plt.xticks(
    [0, K[-1], 2 * K[-1], bands_plot[-1]], ["$\Gamma$", "$X$", "$M$", "$\Gamma$"]
)
plt.axvline(K[-1], c="k", lw=0.3)
plt.axvline(2 * K[-1], c="k", lw=0.3)
plt.ylabel(r"Frequency $\omega a/2\pi c$")
plt.legend([plotTE[0], plotTE_RBME[0]], ["full", "2-point RBME"], loc=(0.31, 0.02))
plt.title("TE modes", c="#cf5268")
plt.tight_layout()

plot_bz(ax[0],"#cf5268")
##############################################################################
# TM polarization:
plt.sca(ax[1])
plotTM = plt.plot(bands_plot, BD["TM"], c="#4199b0", lw=1.5, alpha=0.5)
plotTM_RBME = plt.plot(bands_plot, BD_RBME["TM"], "--", c="#4199b0")
plt.ylim(0, 1.2)
plt.xlim(0, bands_plot[-1])
plt.xticks(
    [0, K[-1], 2 * K[-1], bands_plot[-1]], ["$\Gamma$", "$X$", "$M$", "$\Gamma$"]
)
plt.axvline(K[-1], c="k", lw=0.3)
plt.axvline(2 * K[-1], c="k", lw=0.3)
plt.ylabel(r"Frequency $\omega a/2\pi c$")
plt.legend([plotTM[0], plotTM_RBME[0]], ["full", "2-point RBME"], loc=(0.31, 0.02))
plt.title("TM modes", c="#4199b0")
plt.tight_layout()


plot_bz(ax[1],"#4199b0")



# axins.set_xlim(0,1)
# axins.set_ylim(0,1)
# axins.set_xticks([0,1],["$\Gamma$","X"])
# axins.set_yticks([0,1],["","M"])


os.makedirs(f"figs", exist_ok=True)
plt.savefig(f"figs/figureA3.eps")
plt.savefig(f"figs/figureA3.png")
