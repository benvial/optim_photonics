#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# License: MIT

"""
Topology optimization
=====================

Design of 2D photonic crystals: dispersion engineering and maximizing bandgaps.
"""


import os
import sys
import argparse
import copy
import warnings
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import numpy as np
import protis as pt
from nannos.plot import plot_layer


# matplotlib config
plt.ion()
plt.close("all")
warnings.filterwarnings("ignore")

################################################################
# Create data directory
datadir = "data/protis"
os.makedirs(datadir, exist_ok=True)

################################################################
# Set autodifferentiable backend
pt.set_backend("torch")
bk = pt.backend
no = pt.optimize

################################################################
# Function to save figures
def savefig(base, it):
    name = datadir + "/" + base + str(it).zfill(4) + ".png"
    plt.savefig(name)


def _symmetrize_pattern(dens, x=False, y=True, s8=False):
    dens = bk.array(dens)
    if x:
        dens = 0.5 * (dens + bk.fliplr(dens))
    if y:
        dens = 0.5 * (dens + bk.flipud(dens))
    if s8:
        dens = 0.5 * (dens + dens.T)
    return dens


################################################################
# Path in k-space
def k_space_path(Nb, a):
    K = np.linspace(0, np.pi / a, Nb)
    bands = np.zeros((3 * Nb - 3, 2))
    bands[:Nb, 0] = K
    bands[Nb : 2 * Nb, 0] = K[-1]
    bands[Nb : 2 * Nb - 1, 1] = K[1:]
    bands[2 * Nb - 1 : 3 * Nb - 3, 0] = bands[2 * Nb - 1 : 3 * Nb - 3, 1] = np.flipud(
        K
    )[1:-1]
    return bands, K


################################################################
# Path in k-space (for plotting)
def k_space_path_plot(Nb, K):
    bands_plot = np.zeros(3 * Nb - 2)
    bands_plot[:Nb] = K
    bands_plot[Nb : 2 * Nb - 1] = K[-1] + K[1:]
    bands_plot[2 * Nb - 1 : 3 * Nb - 2] = 2 * K[-1] + 2**0.5 * K[1:]
    return bands_plot


################################################################
# Parse arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    "-p",
    "--polarization",
    default="TE",
    help="polarization case (TE or TM)",
    choices=["TE", "TM"],
)
parser.add_argument(
    "-o",
    "--opt",
    default="bandgap",
    help="type of optimization (bandgap or dispersion)",
    choices=["bandgap", "dispersion"],
)
parser.add_argument(
    "-i",
    "--init",
    default="random",
    help="initial guess (rod, hole or random)",
    choices=["rod", "hole", "random"],
)
parser.add_argument(
    "-e",
    "--eig",
    default=2,
    type=int,
    help="target eigenvalue",
)
args = parser.parse_args()
polarization = args.polarization
type_opt = args.opt
init = args.init
ieig = args.eig

################################################################
# Lattice and PWEM parameters
a = 1
Nx, Ny = 2**8, 2**8
lattice = pt.Lattice([[a, 0], [0, a]], discretization=(Nx, Ny))
eps_min, eps_max = 1, 9
nh = 200
Nb = 101
# N_RBME=None
N_RBME = 8
Nmodel = 2

################################################################
# Optimization parameters
plots = True
rfilt = Nx / 20
maxiter = 40
threshold = (0, 8)
stopval = None

################################################################
# Force symmetry
def symmetrize_pattern(dens):
    if type_opt == "bandgap":
        return _symmetrize_pattern(dens, x=True, y=True, s8=True)
    else:
        return _symmetrize_pattern(dens, x=False, y=True, s8=False)


################################################################
# Initial value
def initialize_density(init):
    nvar = Nx * Ny

    r0 = 0.1
    circ = lattice.circle(center=(0.5, 0.5), radius=r0)

    if init == "rod":
        x0 = lattice.ones() * 0
        x0[circ] = 1

    elif init == "hole":
        x0 = lattice.ones() * 1
        x0[circ] = 0
    elif init == "random":
        np.random.seed(13)
        x0 = bk.array(np.random.rand(nvar))
        x0 = bk.reshape(x0, lattice.discretization)
        # x0 = no.apply_filter(x0, rfilt * 2)
        # x0 = (x0 - x0.min()) / (x0.max() - x0.min())
        x0 = symmetrize_pattern(x0).ravel()

    else:
        raise ValueError
    return x0


bands, K = k_space_path(Nb, a)
bands_plot = k_space_path_plot(Nb, K)
if type_opt == "dispersion":
    bands = bands[:Nb]
    bands_plot = bands_plot[:Nb]
bands_plot = bk.array(bands_plot)

### target dispersion
ks = bands_plot[:Nb]
dispersion = (
    -0.02 * bk.cos(ks * a) + 0.01 * bk.cos(2 * ks * a) + 0.007 * bk.cos(3 * ks * a)
)
omega_c0 = None


if plots:
    fig_bg, ax_bg = plt.subplots(1, figsize=(3.0, 3.1))
    ax_ins = inset_axes(
        ax_bg,
        width="100%",
        height="100%",
        bbox_to_anchor=(0.34, 0.001, 0.2, 0.2),
        bbox_transform=ax_bg.transAxes,
        loc=8,
    )
color = "#6e8cd0" if polarization == "TE" else "#d0716e"
cmap = "Reds" if polarization == "TM" else "Blues"


################################################################
# Function to compute bands


def compute_bands(bands, epsilon, polarization, nh=nh, Nmodel=Nmodel, N_RBME=N_RBME):

    sim = pt.Simulation(lattice, epsilon=epsilon, mu=1, nh=nh)
    q = pt.pi / a
    if Nmodel == 2:
        bands_RBME = [(0, 0), (q, 0), (q, q)]
    elif Nmodel == 3:
        bands_RBME = [(0, 0), (q / 2, 0), (q, 0), (q, q / 2), (q, q), (q / 2, q / 2)]
    else:
        raise ValueError
    rbme = {}
    if N_RBME is not None:
        rbme[polarization] = sim.get_rbme_matrix(N_RBME, bands_RBME, polarization)
    else:
        rbme[polarization] = None

    ev_band = []
    for kx, ky in bands:
        sim.k = kx, ky
        sim.solve(polarization, vectors=False, rbme=rbme[polarization])
        ev_norma = sim.eigenvalues * a / (2 * np.pi)
        ev_band.append(ev_norma)
    # append first value since this is the same point
    if type_opt == "bandgap":
        ev_band.append(ev_band[0])
    band_diag = pt.backend.stack(ev_band).real
    return band_diag, sim


################################################################
# Simulation finction


def simu(x, proj_level=None, rfilt=0, return_bg=False, N_RBME=N_RBME, plots=plots):
    global omega_c
    dens = bk.reshape(x, lattice.discretization)

    dens = symmetrize_pattern(dens)
    density_f = no.apply_filter(dens, rfilt)
    density_fp = (
        no.project(density_f, proj_level) if proj_level is not None else density_f
    )
    epsilon = no.simp(density_fp, eps_min, eps_max, p=1)

    kx, ky = 0, 0
    band_diag, sim = compute_bands(
        bands, epsilon, polarization, nh=nh, Nmodel=Nmodel, N_RBME=N_RBME
    )

    # BG = BG/center

    if type_opt == "dispersion":
        omega_c = bk.mean(band_diag[:, ieig]) if omega_c0 is None else omega_c0
        objective = bk.mean((band_diag[:, ieig] - dispersion - omega_c) ** 2)
        objective = bk.log10(objective)
    else:

        width = bk.min(band_diag[:, ieig + 1]) - bk.max(band_diag[:, ieig])
        center = (bk.min(band_diag[:, ieig + 1]) + bk.max(band_diag[:, ieig])) / 2
        objective = -width
        # objective = 1/width

    if plots:
        is_grad_pass = objective.grad_fn is not None

        if not is_grad_pass:
            BD_plot = band_diag.detach().numpy()
            density_fp_plot = density_fp.detach().numpy()
            plt.sca(ax_bg)
            ax_bg.clear()
            plot_bd(BD_plot, density_fp_plot, ax_bg, ax_ins)

        plt.pause(0.1)

    if return_bg:
        return objective, band_diag
    else:
        return objective


def bg_metrics(BG, ieig=ieig, verbose=False):
    BG = bk.array(BG)
    width = bk.min(bk.real(BG[:, ieig + 1]) - bk.max(BG[:, ieig]))
    center = (bk.min(bk.real(BG[:, ieig + 1])) + bk.max(bk.real(BG[:, ieig]))) / 2
    if verbose:
        print("bandgap center", center.tolist())
        print("bandgap width", width.tolist())
        print("relative width", (width / center).tolist())
    return width, center


itplot = 0


def plot_bd(
    band_diag,
    density_fp,
    ax,
    ax_ins,
    polarization=polarization,
    type_opt=type_opt,
    ieig=ieig,
):
    global itplot
    global omega_c
    gamma_plt = 0.2

    bands, K = k_space_path(Nb, a)
    bands_plot = k_space_path_plot(Nb, K)
    if type_opt == "dispersion":
        bands = bands[:Nb]
        bands_plot = bands_plot[:Nb]

    color = "#6e8cd0" if polarization == "TE" else "#d0716e"

    cmap = "Reds" if polarization == "TM" else "Blues"

    ax.set_title(f"{polarization} modes", c=color)
    if type_opt == "dispersion":
        bands_plot = bands_plot[:Nb]
        omega_c = np.mean(band_diag[:, ieig]) if omega_c0 is None else omega_c0
        ax.plot(
            ks / np.pi / a,
            dispersion + omega_c,
            "--",
            c="k",
            alpha=0.2,
            lw=1.4,
            label="target",
        )

        plot_bands = ax.plot(bands_plot / np.pi / a, band_diag, "-", c=color)
        ax.plot(ks / np.pi / a, band_diag[:, ieig], "-", c="k", lw=0.75)
        ax.set_ylim((1 - gamma_plt) * omega_c, (1 + gamma_plt) * omega_c)
        ax.legend(loc="upper left")
    else:
        width, center = bg_metrics(band_diag, ieig=ieig, verbose=True)
        y1 = np.min(band_diag[:, ieig + 1].real)
        y0 = np.max(band_diag[:, ieig].real)
        # print(y0,y1)
        if y1 > y0:
            ax.fill_between(bands_plot, y1, y0, alpha=0.1, color=color, lw=0)
            ax.hlines(
                center,
                bands_plot[0],
                bands_plot[-1],
                alpha=0.5,
                color=color,
                lw=0.4,
                ls="--",
            )
            ax.annotate(
                rf"$\Delta\omega/\omega_0={100*width/center:0.1f}$%",
                (bands_plot[1], center * (1.02)),
            )
            ax.annotate(
                r"$\omega_0=" + rf"{center:0.3f}$",
                (bands_plot[-1] * 0.84, center * (1.02)),
            )
        ax.set_ylim(0, 1.2)
        plot_bands = ax.plot(bands_plot, band_diag, "-", c=color)

        ax.set_xlim(0, bands_plot[-1])

    if type_opt == "bandgap":
        ax.set_xticks(
            [0, K[-1], 2 * K[-1], bands_plot[-1]],
            ["$\Gamma$", "$X$", "$M$", "$\Gamma$"],
        )
        ax.axvline(K[-1], c="k", lw=0.3)
        ax.axvline(2 * K[-1], c="k", lw=0.3)
        ax.set_ylim(0, 1)
    else:
        ax.set_xlabel("$k_x$ $[\pi/a]$")
        ax.set_xlim(0, 1)
    ax.set_ylabel(r"Frequency $\omega$ $[2\pi c/a]$")
    # ax.legend([plot_bands[0]], ["full", "2-point RBME"], loc=(0.31, 0.02))
    plt.tight_layout()
    ax_ins.clear()
    grid = lattice.unit_grid
    lattice1 = copy.copy(lattice)
    lattice1.basis_vectors = bk.array(lattice.basis_vectors, dtype=bk.float32)
    ims = plot_layer(
        lattice1,
        grid,
        density_fp,
        nper=1,
        cmap=cmap,
        show_cell=True,
        cellstyle="k-",
        ax=ax_ins,
    )
    ax_ins.set_xlim(0, a)
    ax_ins.set_ylim(0, a)
    ax_ins.set_axis_off()
    savefig(f"bd_{type_opt}_{polarization}", itplot)
    itplot += 1


################################################################
# Define callback function

it = 0
history = []


def callback(x, y, proj_level, rfilt):
    global it
    global history
    it += 1
    history.append(float(y))
    return y


##############################################################################
# Optimization class


def initialize_optimizer(x0):
    opt = no.TopologyOptimizer(
        simu,
        x0,
        method="nlopt",
        maxiter=maxiter,
        threshold=threshold,
        stopval=stopval,
        args=(1, rfilt),
        options={},
        callback=callback,
        verbose=True,
    )
    return opt


################################################################
# Main function


def main():
    # initialize density
    x0 = initialize_density(init)
    x0 = x0.real.ravel()
    # optimize
    opt = initialize_optimizer(x0)
    x_opt, f_opt = opt.minimize()
    x_opt = bk.array(x_opt)
    # binary design
    dens = bk.reshape(x_opt, lattice.discretization)
    dens = symmetrize_pattern(dens)
    density_f = no.apply_filter(dens, rfilt)
    density_bin = density_f
    density_bin[density_f < 0.5] = 0
    density_bin[density_f >= 0.5] = 1
    x_bin = density_bin.ravel()
    epsilon_bin = no.simp(density_bin, eps_min, eps_max, p=1)

    # final objective with full model
    obj, band_diag = simu(
        x_bin, proj_level=None, rfilt=0, return_bg=True, N_RBME=None, plots=False
    )
    # final objective with RBME
    obj_approx, band_diag_approx = simu(
        x_bin, proj_level=None, rfilt=0, return_bg=True, N_RBME=8, plots=False
    )
    # save data
    np.savez(
        f"{datadir}/bd_opt_{type_opt}_{polarization}.npz",
        band_diag=band_diag,
        band_diag_approx=band_diag_approx,
        obj_approx=obj_approx,
        obj=obj,
        history=history,
        density_bin=density_bin,
    )


################################################################
#

if __name__ == "__main__":
    main()
