#!/usr/bin/env python
# """
# Set all headers to level 1
# """
#
# from panflute import *
#
# def action(elem, doc):
#     if isinstance(elem, Header):
#         elem.level = 1
#
# def main(doc=None):
#     return run_filter(action, doc=doc)
#
# if __name__ == '__main__':
#     main()


"""
Pandoc filter using panflute
"""

import panflute as pf

import re

def prepare(doc):
    # print(r"a")
    pass


i=1
all_labels = dict()

def action(elem, doc):
    global i
    # if isinstance(elem, pf.Element) and doc.format == 'html':
    #     if isinstance(elem, pf.Div):
    #         pf.debug(elem.classes)
        # pass
        # return None -> element unchanged
        # return [] -> delete element
    # if isinstance(elem, pf.Link) and doc.format == 'html':
    #     pf.debug(elem)
    # pf.debug(elem)
    if isinstance(elem, pf.Math) and doc.format == 'html':
        if "\label" in elem.text and elem.format=="DisplayMath":
            text = elem.text
            # pf.debug(elem)    
            # elem.text = text.replace("\\label{","\\tag{")
            txt = re.findall('\\\label{?(.*?)}',elem.text,flags=re.DOTALL)
            # pf.debug(txt[0])    
            elem.text=re.sub('\\\label{?(.*?)}',f'\\\\tag{{{i}}}',elem.text,flags=re.DOTALL)
            
            # elem.text += f'<span class="bidon" id="{txt[0]}"></span>'
            
            all_labels[str(txt[0])]=i
            i += 1
            # pf.debug(all_labels)
            # pf.debug(elem.container)    
            div = pf.Span(elem,identifier=f"{txt[0]}")
            return div
    if isinstance(elem, pf.Div) and doc.format == 'html':
        if elem.classes[0]=='adjustwidth':
             return []

def finalize(doc):
    pass


def action2(elem, doc):
    lab = list(all_labels.keys())
    if doc.format == 'html':
        if isinstance(elem, pf.Str): 
            if elem.text[1:-1] in lab:
                return pf.Str(str(all_labels[elem.text[1:-1]]))



def main(doc=None):
    return pf.run_filters([action,action2],
                         prepare=prepare,
                         finalize=finalize,
                         doc=doc)


if __name__ == '__main__':
    main()
