#!/usr/bin/env python

import re

# Opening our text file in read only
# mode using the open() function
with open(r'src/article.tex', 'r') as file:

    # Reading the content of the file
    # using the read() function and storing
    # them in a new variable
    data = file.read()

    # Searching and replacing the text
    # using the replace() function
    data = data.replace(r"\AuthorNames", r"\author")
    data = data.replace(r"\Title", r"\title")
    abstract = re.findall(r'\\abstract{?(.*?)}', data, re.DOTALL)[0]
    abstract = abstract.replace ("\n","")
    
    fig = re.findall(r'{figs/(.*?.eps)}', data, re.DOTALL)#[0]
    
    for f in fig:
        f1 = f.split(".")[0]
        
        data = data.replace(rf'{{figs/{f1}.eps}}', rf'{{figs/{f1}.png}}')
    


# Opening our text file in write only
# mode to write the replaced content
with open(r'build/tmptex.tex', 'w') as file:

    # Writing the replaced data in our
    # text file
    file.write(data)



affiliation = "Queen Mary University of London, London E1 4NS, United Kingdom"

with open(r'build/metadata.yml', 'w') as file:

    meta = f"""affiliation: {affiliation}\nabstract: '{abstract}'"""
    file.write(meta)
